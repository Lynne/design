This document is intended to provide a list of variables that apply to the vanilla theme, as well as how these variables map to the elements by default.  
These variables, just like the vanilla theme, are meant to be extended, overriden and changed to achieve the desired look and feel of a live application.

# Variables

## Colors

|variable|comments|
|---|---|
|`colorPrimary`|Indicates a primary call to action
|`colorSecondary`|Default color for non-primary actions
|`colorFurniture`|Meant to be applied to elements that indicate content division
|`colorBorder`|For borders around form elements
|`colorBackgroundHue` | Used to create a discrete contrast the default background color
|`colorSuccess`| Used to indicate a successful validation state
|`colorError`| Used to indicate an error in validation
|`colorText`| Default font color
|`colorTextReverse`| Reverse font color
|`colorTextPlaceholder`| Used for text field placeholders

## Typography

|variable|comments
|---|--
|`fontInterface`|Used for user interface elements by default
|`fontHeading`| Used for headings
|`fontReading`| Main reading text
|`fontWriting`| Font used for writing
|`fontSizeBase`|Default font size
|`fontSizeBaseSmall`| Smaller variation of fontSizeBase
|`fontSizeHeading1`| Size for Heading 1
|`fontSizeHeading2`|Size for Heading 2
|`fontSizeHeading3`|Size for Heading 3
|`fontSizeHeading4`|Size for Heading 4
|`fontSizeHeading5`|Size for Heading 5
|`fontSizeHeading6`|Size for Heading 6
|`fontLineHeight`| Default line height

## Spacing

|variable|comments
|---|---
|`gridUnit`|Base interface space measurement used by elements and typography

## Border

|variable|comments
|---|---
`borderRadius`| Radius value applied to borders throughout the user interface
`borderWidth`|Width value applied to borders
`borderStyle`|Style applied to borders (eg. solid, dashed)

## Shadow
|variable|comments
|---|---
`dropShadow`| Default shadow that is applied to elements that float (eg. tooltips, modals)

## Transition
|variable|comments
|---|---
`transitionDuration`|How long transitions should last
`transitionTimingFunction`|Which function should be applied to transitions (eg. `easein`)
`transitionDelay`|How long transitions should be delayed before they begin



# Application of variables to elements

#### Input / Textarea
`borderStyle`  
`borderWidth`  
`borderRadius`  

`colorBorder`  
`colorText` (for text inside input, as well as the label)  
`colorTextPlaceholder`  

`colorSuccess`  
`colorError`  
These will indicate validation state by changing the `border-color` property, as well as by coloring feedback text.  

`fontSizeBaseSmall` (applicable to the label)  

#### Select Menu
`borderStyle`  
`borderWidth`  
`borderRadius`  

`colorBorder` (non-focused border)  
`colorBackgroundHue` (hovering over an item in the select menu)  
`colorFurniture` (dividing line between text and arrow)  
`colorText`  

#### Radio
`borderWidth`  
`colorText` (applied on the label, as well as for the fill color when the radio button is selected)  

#### Divider
`borderWidth`  
`colorFurniture`  

#### Button
`borderWidth`  
`borderRadius`  

`colorPrimary` (if the button is primary)  
`colorSecondary`  

`colorText` (applied on text for secondary buttons)  
`colorTextReverse` (applied on text for primary buttons)  

#### Tooltip
`borderRadius`  

`colorText` (background color)  
`colorTextReverse` (text color)  

`fontSizeBaseSmall`  
`dropShadow`  

#### Tabs
`borderWidth`  
`borderRadius`  

`colorBorder`  
`colorText`  

#### Checkbox
`borderRadius`  
`borderWidth`  

`colorText` (fill color when it is checked, as well as the label)  

#### Paragraph
`fontReading`  

