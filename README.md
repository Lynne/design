# Pubsweet Design

The purpose of this repo is to document the design principles that will govern pubsweet applications, as well as the processes that lead to those principles.  

## Roadmap

The current task is to tackle theming. For this to happen, it has been decided that we need to decide on a list of small components (atoms) that will be used in pubsweet applications,
and come up with a list of variables that would cover all their use cases in a theme.

This repo is very new. Expect the documentation to grow longer. :)



## On theming and user stories


To put things into perspective, the theming side of the following user stories should be covered by the list of variables: 


*A journal could take an end-to-end solution defined by us that is based on a typical workflow and use it with no customisation (turn key style option)*

Technical implications: None. They will just use the vanilla theme.


*A journal could take set controls created by any participants and use them to match or create their own workflow*

Technical implications: They could lightly or heavily edit the variables to adjust the look and feel of the application to exactly match their organisation's signature branding.


*A journal could take set controls created by Coko, eLife and/or Hindawi and use them to match or create their own workflow and create new bespoke controls of their own (these could also be contributed back to the project if desired)*

Technical implications: Same as the previous story, but they could add custom CSS in their theme that targets these new controls. 


*An individual or organisation may want to take a page control for use outside of xPub*

Technical implications: As long as this control is used within the pubsweet universe, it will be using the same theming mechanism. This means that the same control with the same theme (within pubsweet), will look the same in two different pubsweet apps (eg. a version of XPub and Editoria)


More on theming [here](https://gitlab.coko.foundation/xpub/xpub/issues/88)